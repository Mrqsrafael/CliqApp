FROM node:latest

COPY package.json .

COPY . .

RUN yarn install

RUN ["yarn", "run", "test"]