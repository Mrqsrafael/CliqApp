FROM reactnativecommunity/react-native-android

WORKDIR /metro

COPY package.json .

COPY . .

RUN yarn install --silent

EXPOSE 3000

CMD ["yarn", "run", "start"]